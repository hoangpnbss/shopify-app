import React, { useState, useCallback } from "react";
import {
  Card,
  Stack,
  FormLayout,
  RadioButton,
  TextField,
} from "@shopify/polaris";
import store from "store-js";
import ProductTag from "./ProductTag";
import { ResourcePicker } from "@shopify/app-bridge-react";
import SpecificProduct from "./SpecificProduct";

const ProductSetting = ({
  applyToProduct,
  setApplyToProduct,
  setSelectedProduct,
  setSelectedCollection,
  setSelectedProductTag,
  setError,
}) => {
  // RESOURCE PICKER TOGGLE OPEN
  const [productPickerOpen, setProductPickerOpen] = useState(false);
  const [collectionPickerOpen, setCollectionPickerOpen] = useState(false);

  const handleApplyToProduct = useCallback(
    (_checked, value) => setApplyToProduct(value),
    []
  );

  const handleProductSelection = (resources) => {
    const productTitle = resources.selection.map((product) => {
      return product.title;
    });
    console.log(productTitle);
    // setSelectedProduct(productTitle);
    setProductPickerOpen(false);
  };

  const handleCollectionSelection = (resources) => {
    const collectionTitle = resources.selection.map((collection) => {
      return collection.title;
    });
    console.log(collectionTitle);
    setCollectionPickerOpen(false);
  };
  return (
    <Card title="Apply to Products" sectioned>
      <FormLayout>
        <Stack vertical>
          <RadioButton
            label="All products"
            id="all_products"
            checked={applyToProduct === "all_products"}
            name="products"
            onChange={handleApplyToProduct}
          />

          <RadioButton
            label="Specific products"
            id="specific_products"
            name="products"
            checked={applyToProduct === "specific_products"}
            onChange={handleApplyToProduct}
          />
          {
            applyToProduct === "specific_products" && (
              <TextField
                label="Search specific products"
                labelHidden={true}
                placeholder="Search product"
                autoComplete="off"
                onFocus={() => setProductPickerOpen(true)}
              />
            )
            // <SpecificProduct setSelectedProduct={setSelectedProduct} />
          }

          <RadioButton
            label="Product collections"
            id="product_collections"
            name="products"
            checked={applyToProduct === "product_collections"}
            onChange={handleApplyToProduct}
          />

          {applyToProduct === "product_collections" && (
            <TextField
              label="Search collections"
              labelHidden={true}
              placeholder="Search collections"
              autoComplete="off"
              onFocus={() => setCollectionPickerOpen(true)}
            />
          )}
          <RadioButton
            label="Product tags"
            id="product_tags"
            name="products"
            checked={applyToProduct === "product_tags"}
            onChange={handleApplyToProduct}
          />
          {applyToProduct === "product_tags" && (
            <ProductTag setSelectedProductTag={setSelectedProductTag} />
          )}
        </Stack>
      </FormLayout>
      {!collectionPickerOpen && (
        <ResourcePicker
          resourceType="Product"
          showVariants={false}
          open={productPickerOpen}
          onSelection={(resources) => handleProductSelection(resources)}
          onCancel={() => setProductPickerOpen(false)}
        />
      )}
      {!productPickerOpen && (
        <ResourcePicker
          resourceType="Collection"
          open={collectionPickerOpen}
          onSelection={(resources) => handleCollectionSelection(resources)}
          onCancel={() => setCollectionPickerOpen(false)}
        />
      )}
    </Card>
  );
};

export default ProductSetting;
