import { useState, useCallback } from "react";
import { useQuery } from "react-apollo";
import {
  Stack,
  Autocomplete,
  TextContainer,
  Tag,
  Spinner,
  Card,
} from "@shopify/polaris";
import { GET_FIRST_FIFTY_PRODUCTS } from "../queries";

const SpecificProduct = ({ setSelectedProduct }) => {
  const { loading, data, error } = useQuery(GET_FIRST_FIFTY_PRODUCTS);

  const deselectedOptions = [];
  const [selectedOptions, setSelectedOptions] = useState(
    !store.get("products") ? [] : store.get("products")
  );
  const [inputValue, setInputValue] = useState("");
  const [options, setOptions] = useState(deselectedOptions);

  const updateText = useCallback(
    (value) => {
      setInputValue(value);

      if (value === "") {
        setOptions(deselectedOptions);
        return;
      }

      const filterRegex = new RegExp(value, "i");
      const resultOptions = deselectedOptions.filter((option) =>
        option.label.match(filterRegex)
      );
      let endIndex = resultOptions.length - 1;
      if (resultOptions.length === 0) {
        endIndex = 0;
      }
      setOptions(resultOptions);
    },
    [deselectedOptions]
  );

  const removeTag = useCallback(
    (tag) => () => {
      const options = [...selectedOptions];
      options.splice(options.indexOf(tag), 1);
      store.set("tags", options);
      setSelectedOptions(options);
      setSelectedProductTag(options);
    },
    [selectedOptions]
  );

  const tagsMarkup = selectedOptions.map((option) => {
    let tagLabel = "";
    tagLabel = option.replace("_", " ");
    tagLabel = titleCase(tagLabel);
    return (
      <Tag key={`option${option}`} onRemove={removeTag(option)}>
        {tagLabel}
      </Tag>
    );
  });

  function titleCase(string) {
    return string
      .toLowerCase()
      .split(" ")
      .map((word) => word.replace(word[0], word[0].toUpperCase()))
      .join("");
  }

  //   const textField = (
  //     <Autocomplete.TextField
  //       onChange={updateText}
  //       label="Products"
  //       labelHidden
  //       value={inputValue}
  //       placeholder="14k, 18k"
  //     />
  //   );

  const handleSelectProduct = useCallback((selected) => {
    setSelectedOptions(selected);
    setSelectedProduct(selected);
    store.set("products", selected);
  }, []);

  if (loading) return <Spinner />;
  if (error) return <div>{error.message}</div>;

  console.log(data);

  data.products.edges.forEach((product) => {
    deselectedOptions.push({
      id: product.node.id,
      title: product.node.title,
      image: product.node.featuredImage.originalSrc,
    });
  });

  return (
    <>
      <Card>
        <ResourceList
          showHeader
          resourceName={{ singular: "Product", plural: "Products" }}
          items={data.nodes}
          selectedItems={selectedOptions}
          onSelectionChange={handleSelectProduct}
          selectable
          renderItem={(item) => {
            const media = (
              <Thumbnail
                source={
                  item.images.edges[0] ? item.images.edges[0].node.src : ""
                }
                alt={
                  item.images.edges[0] ? item.images.edges[0].node.altText : ""
                }
              />
            );
            const price = item.variants.edges[0].node.price;
            return (
              <ResourceList.Item id={item.id} media={media}>
                <Stack>
                  <Stack.Item fill>
                    <h3>
                      <TextStyle variation="strong">{item.title}</TextStyle>
                    </h3>
                  </Stack.Item>
                  <Stack.Item>
                    <p>{price}</p>
                  </Stack.Item>
                </Stack>
              </ResourceList.Item>
            );
          }}
        />
      </Card>
      <br />
      <TextContainer>
        <Stack>{tagsMarkup}</Stack>
      </TextContainer>
    </>
  );
};

export default SpecificProduct;
