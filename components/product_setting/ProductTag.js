import { useState, useCallback } from "react";
import { useQuery } from "react-apollo";
import {
  Stack,
  Autocomplete,
  TextContainer,
  Tag,
  Spinner,
} from "@shopify/polaris";
import store from "store-js";
import { GET_PRODUCT_TAGS } from "../queries";

const ProductTag = ({ setSelectedProductTag }) => {
  const { loading, error, data } = useQuery(GET_PRODUCT_TAGS);
  const deselectedOptions = [];
  const [selectedOptions, setSelectedOptions] = useState(
    !store.get("tags") ? [] : store.get("tags")
  );
  const [inputValue, setInputValue] = useState("");
  const [options, setOptions] = useState(deselectedOptions);

  const updateText = useCallback(
    (value) => {
      setInputValue(value);

      if (value === "") {
        setOptions(deselectedOptions);
        return;
      }

      const filterRegex = new RegExp(value, "i");
      const resultOptions = deselectedOptions.filter((option) =>
        option.label.match(filterRegex)
      );
      let endIndex = resultOptions.length - 1;
      if (resultOptions.length === 0) {
        endIndex = 0;
      }
      setOptions(resultOptions);
    },
    [deselectedOptions]
  );

  const removeTag = useCallback(
    (tag) => () => {
      const options = [...selectedOptions];
      options.splice(options.indexOf(tag), 1);
      store.set("tags", options);
      setSelectedOptions(options);
      setSelectedProductTag(options);
    },
    [selectedOptions]
  );

  const tagsMarkup = selectedOptions.map((option) => {
    let tagLabel = "";
    tagLabel = option.replace("_", " ");
    tagLabel = titleCase(tagLabel);
    return (
      <Tag key={`option${option}`} onRemove={removeTag(option)}>
        {tagLabel}
      </Tag>
    );
  });

  function titleCase(string) {
    return string
      .toLowerCase()
      .split(" ")
      .map((word) => word.replace(word[0], word[0].toUpperCase()))
      .join("");
  }

  const textField = (
    <Autocomplete.TextField
      onChange={updateText}
      label="Tags"
      labelHidden
      value={inputValue}
      placeholder="Vintage, cotton, summer"
    />
  );

  const handleSelectTag = useCallback((selected) => {
    setSelectedOptions(selected);
    setSelectedProductTag(selected);
    store.set("tags", selected);
  }, []);

  if (loading) return <Spinner />;
  if (error) return <div>{error.message}</div>;

  data.shop.productTags.edges.forEach((tag) => {
    deselectedOptions.push({
      label: tag.node,
      value: tag.node,
      cursor: tag.cursor,
    });
  });

  return (
    <div>
      <Autocomplete
        allowMultiple
        options={options}
        selected={selectedOptions}
        textField={textField}
        onSelect={handleSelectTag}
        listTitle="SUGGESTED TAGS"
      />
      <br />
      <TextContainer>
        <Stack>{tagsMarkup}</Stack>
      </TextContainer>
    </div>
  );
};

export default ProductTag;
