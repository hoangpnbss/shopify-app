import { Card, ChoiceList, FormLayout, TextField } from "@shopify/polaris";
import { useCallback, useState } from "react";
const PriceSetting = ({
  amount,
  setAmount,
  selectedCustomPrice,
  setSelectedCustomPrice,
}) => {
  const handleAmountChange = useCallback((value) => setAmount(value), []);

  const handleCustomPriceChange = useCallback((value) => {
    setSelectedCustomPrice(value);
  }, []);
  return (
    <Card title="Custom Prices" sectioned>
      <FormLayout>
        <ChoiceList
          title="Custom prices"
          titleHidden
          choices={[
            {
              label: "Apply a price to selected products",
              value: "fixed_amount",
            },
            {
              label:
                "Decrease a fixed amount of the original prices of selected products",
              value: "decrease_fixed_amount",
            },
            {
              label:
                "Decrease the original prices of selected products by a percentage(%)",
              value: "decrease_percentage_amount",
            },
          ]}
          selected={selectedCustomPrice}
          onChange={handleCustomPriceChange}
        />

        <TextField
          label="Amount"
          onChange={handleAmountChange}
          value={amount}
          autoComplete="off"
        />
      </FormLayout>
    </Card>
  );
};

export default PriceSetting;
