import { Card, DataTable } from "@shopify/polaris";
import store from "store-js";

const Table = ({
  applyToProduct,
  selectedProduct,
  selectedCollection,
  selectedProductTag,
  amount,
  selectedCustomPrice,
  setError,
}) => {
  const price =
    selectedCustomPrice[0] === "fixed_amount"
      ? `${amount}đ `
      : selectedCustomPrice[0] === "decrease_fixed_amount"
      ? `all variant prices - ${amount}đ`
      : selectedCustomPrice[0] === "decrease_percentage_amount"
      ? `all variant prices - ${amount}%`
      : "";

  const specificProducts = selectedProduct.map((product) => {
    return [product, price];
  });

  const productCollections = selectedCollection.map((collection) => {
    return [collection, price];
  });

  const productTags = selectedProductTag.map((tag) => {
    return [tag, price];
  });

  return (
    <Card title="Show product pricing details" sectioned>
      {applyToProduct === "all_products" && (
        <DataTable
          columnContentTypes={["text", "text"]}
          headings={["Title", "Modified Price"]}
          rows={[["All product", price]]}
        />
      )}

      {applyToProduct === "specific_products" && (
        <DataTable
          columnContentTypes={["text", "text"]}
          headings={["Title", "Modified Price"]}
          rows={specificProducts}
        />
      )}

      {applyToProduct === "product_collections" && (
        <DataTable
          columnContentTypes={["text", "text"]}
          headings={["Title", "Modified Price"]}
          rows={productCollections}
        />
      )}

      {applyToProduct === "product_tags" && (
        <DataTable
          columnContentTypes={["text", "text"]}
          headings={["Title", "Modified Price"]}
          rows={productTags}
        />
      )}
    </Card>
  );
};

export default Table;
