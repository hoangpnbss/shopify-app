import gql from "graphql-tag";

export const GET_FIRST_FIFTY_PRODUCTS = gql`
  query products {
    products(first: 50) {
      edges {
        node {
          id
          title
          images(first: 1) {
            edges {
              node {
                altText
                src
              }
            }
          }
          variants(first: 1) {
            edges {
              node {
                price
                id
              }
            }
          }
        }
      }
    }
  }
`;

export const GET_PRODUCT_TAGS = gql`
  query Tag {
    shop {
      productTags(first: 250) {
        edges {
          cursor
          node
        }
      }
    }
  }
`;

export const GET_PRODUCT_COLLECTIONS = gql`
  query Collection {
    collections(first: 100) {
      edges {
        node {
          id
          title
          productCount
        }
      }
    }
  }
`;
