import { Card, FormLayout, TextField, Select } from "@shopify/polaris";
import { useState, useCallback } from "react";
import store from "store-js";

const GeneralInfo = ({ name, setName, setError }) => {
  const [priority, setPriority] = useState(
    !store.get("priority") ? "0" : store.get("priority")
  );
  const [status, setStatus] = useState(
    !store.get("status") ? "enable" : store.get("status")
  );

  const handleNameChange = useCallback((value) => {
    setName(value);
    store.set("name", value);
  }, []);

  const handlePriorityChange = useCallback(
    (value) => {
      if (value > 99 || value < 0) {
        setError("Please enter a number from 0 to 99");
      }
      setPriority(value);
      store.set("priority", value);
    },
    [name]
  );

  const handleStatusChange = useCallback(
    (value) => {
      setStatus(value);
      store.set("status", value);
    },
    [status]
  );

  return (
    <Card title="General Information" sectioned>
      <FormLayout>
        <TextField
          value={name}
          label="Name"
          onChange={handleNameChange}
          autoComplete="off"
        />

        <TextField
          label="Priority"
          type="number"
          value={priority}
          onChange={handlePriorityChange}
          helpText="Please enter an integer from 0 to 99. 0 is the highest priority"
          autoComplete="off"
        />

        <Select
          label="Status"
          options={[
            { label: "Enable", value: "enable" },
            { label: "Disable", value: "disable" },
          ]}
          onChange={handleStatusChange}
          value={status}
        />
      </FormLayout>
    </Card>
  );
};

export default GeneralInfo;
