import { useState } from "react";
import store from "store-js";
import { Layout, Page } from "@shopify/polaris";

import GeneralInfo from "../components/GeneralInfo";
import ProductSetting from "../components/product_setting/ProductSetting";
import PriceSetting from "../components/price_setting/PriceSetting";
import Table from "../components/Table";

const Index = () => {
  const [name, setName] = useState(!store.get("name") ? "" : store.get("name")); // rule name
  const [applyToProduct, setApplyToProduct] = useState("all_products");
  const [error, setError] = useState("");
  const [selectedProduct, setSelectedProduct] = useState(
    !store.get("products") ? [] : store.get("products")
  );
  const [selectedCollection, setSelectedCollection] = useState(
    !store.get("collections") ? [] : store.get("collections")
  );
  const [selectedProductTag, setSelectedProductTag] = useState(
    !store.get("tags") ? [] : store.get("tags")
  );
  const [amount, setAmount] = useState(
    !store.get("amount") ? "0" : store.get("amount")
  );
  const [selectedCustomPrice, setSelectedCustomPrice] = useState([
    "fixed_amount",
  ]);

  return (
    <Page title="New Price Rule">
      <Layout>
        <Layout.Section>
          <GeneralInfo name={name} setName={setName} setError={setError} />

          <ProductSetting
            applyToProduct={applyToProduct}
            setApplyToProduct={setApplyToProduct}
            setSelectedProduct={setSelectedProduct}
            setSelectedCollection={setSelectedCollection}
            setSelectedProductTag={setSelectedProductTag}
            setError={setError}
          />

          <PriceSetting
            amount={amount}
            setAmount={setAmount}
            selectedCustomPrice={selectedCustomPrice}
            setSelectedCustomPrice={setSelectedCustomPrice}
          />

          <Table
            applyToProduct={applyToProduct}
            selectedProduct={selectedProduct}
            selectedCollection={selectedCollection}
            selectedProductTag={selectedProductTag}
            amount={amount}
            selectedCustomPrice={selectedCustomPrice}
            setError={setError}
          />
        </Layout.Section>
      </Layout>
    </Page>
  );
};

export default Index;
